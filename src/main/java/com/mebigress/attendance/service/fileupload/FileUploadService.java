package com.mebigress.attendance.service.fileupload;

import java.util.List;

import com.mebigress.attendance.entity.fileupload.AttendanceEntity;
import com.mebigress.attendance.entity.fileupload.EmployeeEntity;
import com.mebigress.attendance.entity.fileupload.WorkingStateEntity;
import com.mebigress.attendance.repositories.fileupload.AttendanceRepository;
import com.mebigress.attendance.repositories.fileupload.EmployeeRepository;
import com.mebigress.attendance.repositories.fileupload.WorkingStateRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileUploadService {

	@Autowired
	AttendanceRepository attendanceRepository;
	@Autowired
	WorkingStateRepository workingStateRepository;
	@Autowired
	EmployeeRepository employeeRepository;

	public void saveWorkingStateEntity(List<WorkingStateEntity> entity) {
		workingStateRepository.saveAll(entity);
	}

	public void saveEmployeeEntity(List<EmployeeEntity> entity) {
		employeeRepository.saveAll(entity);
	}

	public void saveAttendanceEntity(List<AttendanceEntity> entity) {
		attendanceRepository.saveAll(entity);
	}

}

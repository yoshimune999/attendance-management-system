package com.mebigress.attendance.service.createworkschedule;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mebigress.attendance.entity.createworkschedule.RelationalEntity;
import com.mebigress.attendance.repositories.createworkschedule.RelationalRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@PropertySource(value = "classpath:sql/sqlmap.properties")
@Service
public class CreateWrokScheduleService {

	@Autowired
	RelationalRepository relationalRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Value("${sqlmap.selectPersonalSchedule}")
    String SELECT_SQL;

	public List<RelationalEntity> getPersonalSchedule(Date beginning, Date end, int num){
		return entityManager
				.createNativeQuery(SELECT_SQL, RelationalEntity.class)
				.setParameter("num", num)
				.setParameter("beginning", beginning)
				.setParameter("end", end)
				.getResultList();
	}
}

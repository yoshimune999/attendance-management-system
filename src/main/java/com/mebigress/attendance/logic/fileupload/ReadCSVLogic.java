package com.mebigress.attendance.logic.fileupload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.mebigress.attendance.entity.fileupload.AttendanceEntity;
import com.mebigress.attendance.entity.fileupload.EmployeeEntity;
import com.mebigress.attendance.entity.fileupload.WorkingStateEntity;


public class ReadCSVLogic {

	private List<WorkingStateEntity> wEntityList = new ArrayList<WorkingStateEntity>();
	private List<EmployeeEntity> eEntityList = new ArrayList<EmployeeEntity>();
	private List<AttendanceEntity> aEntityList = new ArrayList<AttendanceEntity>();


	public void readCSV(MultipartFile csv) {

		BufferedReader br = null;

		try {

			InputStream fi = csv.getInputStream();
			InputStreamReader is = new InputStreamReader(fi, "SJIS");
			br = new BufferedReader(is);
			String line;
			int i = 0;

			while ((line = br.readLine()) != null) {

				//1行目をスキップ
				if (i == 0) { i++;}

				else {
					//文字コードを変換
					byte[] b = line.getBytes();
					line = new String(b, "UTF-8");

					String[] column = line.split(",",-1);
					createEntity(column);
				}
			 }

		} catch (IOException e) {
			System.out.println(e);
		} finally {

			try {
			br.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	//entityを作成
	private void createEntity(String[] column) {

		WorkingStateEntity wEntity = new WorkingStateEntity();
		EmployeeEntity eEntity = new EmployeeEntity();
		AttendanceEntity aEntity = new AttendanceEntity();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");


		aEntity.setDate(Date.valueOf(LocalDate.parse(column[0], formatter)));

		aEntity.setStaffNumber(Integer.valueOf(column[2]));

		aEntity.setCategoryId(Integer.valueOf(column[4]));

		aEntity.setInTime(column[6]);

		aEntity.setTodayOrNext1(Integer.valueOf(column[7]));

		aEntity.setOutTime(column[8]);

		aEntity.setTodayOrNext2(Integer.valueOf(column[9]));

		aEntity.setRest1(column[10]);

		aEntity.setTodayOrNext3(Integer.valueOf(column[11]));

		aEntity.setRest1End(column[12]);

		aEntity.setTodayOrNext4(Integer.valueOf(column[13]));

		aEntity.setRest2(column[14]);

		aEntity.setTodayOrNext5(Integer.valueOf(column[15]));

		aEntity.setRest2_end(column[16]);

		aEntity.setTodayOrNext6(Integer.valueOf(column[17]));

		aEntity.setExpense(convertBlankToZero(column[18]));

		aEntity.setRemarks(column[19]);

		aEntity.setComment(column[20]);

		aEntity.setApprovalState(column[21]);

		aEntity.setPunchInTime(column[22]);

		aEntity.setPunchOutTime(column[23]);

		aEntity.setPunchRest1(column[24]);

		aEntity.setPunchRest1End(column[25]);

		aEntity.setPunchRest2(column[26]);

		aEntity.setPunchRest2End(column[27]);

		wEntity.setId(Integer.valueOf(column[4]));

		wEntity.setName(column[5]);

		eEntity.setNumber(Integer.valueOf(column[2]));

		eEntity.setName(column[3]);

		wEntityList.add(wEntity);
		eEntityList.add(eEntity);
		aEntityList.add(aEntity);

	}


	//格納する値が空の場合の処理
	private int convertBlankToZero(String element) {

		if (element.equals("")) {
			return 0;
		} else {
			return Integer.valueOf(element);
		}
	}

	public List<WorkingStateEntity> getWorkingStateEntity(){
		return this.wEntityList;
	}

	public List<EmployeeEntity> getEmployeeEntity(){
		return this.eEntityList;
	}

	public List<AttendanceEntity> getAttendanceEntity(){
		return this.aEntityList;
	}

}



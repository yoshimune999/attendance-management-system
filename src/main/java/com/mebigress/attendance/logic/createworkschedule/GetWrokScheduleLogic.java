package com.mebigress.attendance.logic.createworkschedule;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.mebigress.attendance.entity.createworkschedule.RelationalEntity;

@Component
public class GetWrokScheduleLogic {
	FileInputStream fs = null;
	Workbook book = null;
	Sheet sheet = null;
	private int number = 0;
	private String name = "";
	private Date date = null;
	private String workState = "";
	private String inTime = "";
	private String outTime = "";
	private String inRest = "";
	private String outRest = "";
	private String inRest2 = "";
	private String outRest2 = "";
	private String remarks = "";
	private int inHour = 0;
	private int inMinute = 0;
	private int outHour = 0;
	private int outMinute = 0;
	private int inRestHour = 0;
	private int inRestMinute = 0;
	private int outRestHour = 0;
	private int outRestMinute = 0;
	private int inRestHour2 = 0;
	private int inRestMinute2 = 0;
	private int outRestHour2 = 0;
	private int outRestMinute2 = 0;
	private int restHour = 0;
	private int restMinute = 0;
	private int nightRestHour =0;
	private int nightRestMinute =0;
	private int totalRestHour = 0;
	private int totalRestMinute = 0;
	private int todayOrNext = 0;
	private int workingHour = 0;
	private int workingMinute = 0;
	private int totalWorkingHour = 0;
	private int totalWorkingMinute = 0;
	private int totalOverHour = 0;
	private int totalOverMinute = 0;
	private int totalNightHour = 0;
	private int totalNightMinute = 0;
	private int i = 0;


	public Workbook getWrokSchedule(List<RelationalEntity> list) {

		try {

			String path = System.getProperty("user.dir");
			fs = new FileInputStream(path + "/src/main/resources/attendanceExcel/template.xlsx");
			book = new XSSFWorkbook(fs);
			sheet = book.getSheet("勤務表");


			for (i = 0;i<list.size(); i++) {
				RelationalEntity entity = list.get(i);


				//DBから取得した情報を定義
				workState = entity.getCategoryName();
				inTime = entity.getInTime();
				outTime = entity.getOutTime();
				inRest = entity.getRest1();
				outRest = entity.getRest1End();
				inRest2 = entity.getRest2();
				outRest2 = entity.getRest2_end();
				todayOrNext = entity.getTodayOrNext2();
				remarks = entity.getRemarks();
				//1回のみ(社員番号,氏名,日付)を表示
				if(i == 0) {
					number = entity.getStaffNumber();
					name = entity.getName();
					date = entity.getDate();
					sheet.getRow(3).getCell(3).setCellValue("000"+number);
					sheet.getRow(4).getCell(3).setCellValue(name);
					sheet.getRow(6).getCell(1).setCellValue(date);
				}

				//(種別,出社,退社) 列の表示
				sheet.getRow(i+8).getCell(3).setCellValue(workState);
				sheet.getRow(i+8).getCell(4).setCellValue(inTime);
				sheet.getRow(i+8).getCell(5).setCellValue(outTime);
				sheet.getRow(i+8).getCell(12).setCellValue(remarks);

				//休憩時間の表示
				if(inRest.length() == 4) {
					getInRestHMM();
					createRestTime();
				}else if(inRest.length() == 5){
					getInRestHHMM();
					createRestTime();
				}

				//深夜時間の表示
				if(!(inRest2.equals(""))) {
					if(inRest2.length() == 4) {
						getInRest2HMM();
						createRestTime2();
					}else if(inRest2.length() == 5){
						getInRest2HHMM();
						createRestTime2();
					}
					//総休憩時間の算出
					totalRestHour += restHour;
					totalRestMinute += restMinute;
					if(totalRestMinute >= 60) {
						totalRestHour += 1;
						totalRestMinute -= 60;
					}
				}

				//実働時間の表示
				if(inTime.length() == 4) {
					getInHMM();
					createWorkingTime();
				}else if(inTime.length() == 5){
					getInHHMM();
					createWorkingTime();
				}

				//total実働時間の計算
				totalWorkingHour += workingHour;
				totalWorkingMinute += workingMinute;
				if(totalWorkingMinute >= 60) {
					totalWorkingMinute -= 60;
					totalWorkingHour += 1;
				}

				//残業時間の表示
				if(workingHour >= 8) {
					if(workingMinute < 10){
						sheet.getRow(i+8).getCell(9).setCellValue(workingHour-8+":0"+workingMinute);

						//total残業時間の計算
						totalOverHour += workingHour-8;
						totalOverMinute += workingMinute;
						if(totalOverMinute>=60) {
							totalOverMinute -= 60;
							totalOverHour += 1;
						}
					}else {
						sheet.getRow(i+8).getCell(9).setCellValue(workingHour-8+":"+workingMinute);

						//total残業時間の計算
						totalOverHour += workingHour-8;
						totalOverMinute += workingMinute;
						if(totalOverMinute>=60) {
							totalOverMinute -= 60;
							totalOverHour += 1;
						}
					}
				}else {
					sheet.getRow(i+8).getCell(9).setCellValue("0:00");
				}

				//深夜時間(22時から5時の休憩を加味)の表示
				if(outTime.length() == 5) {
					getOutHHMM();

					//深夜休憩時間取得、反映
					getNightRest();
					outHour -= nightRestHour;
					outMinute -= nightRestMinute;
					if(outMinute < 0) {
						outMinute += 60;
						outHour -= 1;
					}

					//深夜時間の表示
					if((outHour == 22)&&(outMinute >= 10)) {
						sheet.getRow(i+8).getCell(10).setCellValue("0:"+outMinute);

						//total深夜時間の計算
						totalNightMinute += outMinute;
						if(totalNightMinute>=60) {
							totalNightMinute -= 60;
							totalNightHour += 1;
						}
					}else if((outHour == 22)&&(outMinute < 0)){
						sheet.getRow(i+8).getCell(10).setCellValue("0:0"+outMinute);

						//total深夜時間の計算
						totalNightMinute += outMinute;
						if(totalNightMinute>=60) {
							totalNightMinute -= 60;
							totalNightHour += 1;
						}
					}else if(outHour>=23){
						if(outMinute <10) {
							sheet.getRow(i+8).getCell(10).setCellValue((outHour-22)+":0"+outMinute);

							//total深夜時間の計算
							totalNightHour += outHour-22;

						}else {
							sheet.getRow(i+8).getCell(10).setCellValue((outHour-22)+":"+outMinute);

							//total深夜時間の計算
							totalNightHour += outHour-22;
							totalNightMinute += outMinute;
							if(totalNightMinute>=60) {
								totalNightMinute -= 60;
								totalNightHour += 1;
							}
						}
					}else {
						sheet.getRow(i+8).getCell(10).setCellValue("0:00");
					}
				}else if(outTime.length() == 4){
					getOutHMM();

					//深夜休憩時間取得、反映
					getNightRest();
					outHour -= nightRestHour;
					outMinute -= nightRestMinute;
					if(outMinute < 0) {
						outMinute += 60;
						outHour -= 1;
					}

					//深夜時間の表示
					if(outHour<5) {
						if(outMinute < 10) {
							sheet.getRow(i+8).getCell(10).setCellValue(outHour+2+":0"+outMinute);
						}else {
							sheet.getRow(i+8).getCell(10).setCellValue(outHour+2+":"+outMinute);
						}

						//total深夜時間の計算
						totalNightHour += outHour + 2;
						totalNightMinute += outMinute;
						if(totalNightMinute>=60) {
							totalNightMinute -= 60;
							totalNightHour += 1;
						}
					}else if((outHour >= 5)) {
						if(nightRestMinute>50) {
							sheet.getRow(i+8).getCell(10).setCellValue((7-nightRestHour)+":0"+(60-nightRestMinute));
							//total深夜時間の計算
							totalNightHour += 7 - nightRestHour;
							totalNightMinute += 60 - nightRestMinute;
							if(totalNightMinute>=60) {
								totalNightMinute -= 60;
								totalNightHour += 1;
							}
						}else if(nightRestMinute>0) {
							sheet.getRow(i+8).getCell(10).setCellValue((7-nightRestHour)+":"+(60-nightRestMinute));
							//total深夜時間の計算
							totalNightHour += 7 - nightRestHour;
							totalNightMinute += 60 - nightRestMinute;
							if(totalNightMinute>=60) {
								totalNightMinute -= 60;
								totalNightHour += 1;
							}
						}else {
						sheet.getRow(i+8).getCell(10).setCellValue((7-nightRestHour)+":00");
						}

						//total深夜時間の計算
						totalNightHour += 7 - nightRestHour;
					}else {
						sheet.getRow(i+8).getCell(10).setCellValue("0:00");
					}
				}
				workingHour = 0;
				workingMinute = 0;

			}
			//total時間の表示
			createTotalCount(8, totalWorkingHour, totalWorkingMinute);
			createTotalCount(9, totalOverHour, totalOverMinute);
			createTotalCount(10, totalNightHour, totalNightMinute);

		} catch (IOException e){
			System.out.println(e);
		}finally{
			try {
				fs.close();
			}catch(IOException e){
				System.out.println(e);
			}
		}
		book.getCreationHelper().createFormulaEvaluator().evaluateAll();
		sheet.setForceFormulaRecalculation(true);

		return book;
	}

	//各桁数でString型からint型へ変換し取得
	private void getInHMM() {
		inHour =  Character.getNumericValue(inTime.charAt(0));
		inMinute = Integer.parseInt(inTime.substring(2, 4));
	}

	private void getInHHMM() {
		inHour = Integer.parseInt(inTime.substring(0, 2));
		inMinute = Integer.parseInt(inTime.substring(3, 5));
	}

	private void getOutHMM() {
		outHour =  Character.getNumericValue(outTime.charAt(0));
		outMinute = Integer.parseInt(outTime.substring(2, 4));
	}

	private void getOutHHMM() {
		outHour = Integer.parseInt(outTime.substring(0, 2));
		outMinute = Integer.parseInt(outTime.substring(3, 5));
	}

	private void getInRestHMM() {
		inRestHour =  Character.getNumericValue(inRest.charAt(0));
		inRestMinute = Integer.parseInt(inRest.substring(2, 4));
	}

	private void getInRestHHMM() {
		inRestHour = Integer.parseInt(inRest.substring(0, 2));
		inRestMinute = Integer.parseInt(inRest.substring(3, 5));
	}

	private void getOutRestHMM() {
		outRestHour =  Character.getNumericValue(outRest.charAt(0));
		outRestMinute = Integer.parseInt(outRest.substring(2, 4));
	}

	private void getOutRestHHMM() {
		outRestHour = Integer.parseInt(outRest.substring(0, 2));
		outRestMinute = Integer.parseInt(outRest.substring(3, 5));
	}
	private void getInRest2HMM() {
		inRestHour2 =  Character.getNumericValue(inRest2.charAt(0));
		inRestMinute2 = Integer.parseInt(inRest2.substring(2, 4));
	}

	private void getInRest2HHMM() {
		inRestHour2 = Integer.parseInt(inRest2.substring(0, 2));
		inRestMinute2 = Integer.parseInt(inRest2.substring(3, 5));
	}

	private void getOutRest2HMM() {
		outRestHour2 =  Character.getNumericValue(outRest2.charAt(0));
		outRestMinute2 = Integer.parseInt(outRest2.substring(2, 4));
	}

	private void getOutRest2HHMM() {
		outRestHour2 = Integer.parseInt(outRest2.substring(0, 2));
		outRestMinute2 = Integer.parseInt(outRest2.substring(3, 5));
	}

	//休憩の表示
	private void createRestTime() {
			if(outRest.length() == 5) {
				getOutRestHHMM();
				totalRestHour = outRestHour -inRestHour;
			}else if(outRest.length() == 4){
				getOutRestHMM();
				totalRestHour = outRestHour+24 -inRestHour;
			}
			totalRestMinute = outRestMinute - inRestMinute;

		if(outRestMinute<inRestMinute) {
			sheet.getRow(i+8).getCell(6).setCellValue((totalRestHour-1)+":"+(totalRestMinute+60));
		}else if(totalRestMinute < 10){
			sheet.getRow(i+8).getCell(6).setCellValue(totalRestHour+":0"+totalRestMinute);
		}else {
			sheet.getRow(i+8).getCell(6).setCellValue(totalRestHour+":"+totalRestMinute);
		}
	}

	//深夜休憩の表示
	private void createRestTime2() {
		if(outRest2.length() == 5) {
			getOutRest2HHMM();
		}else if(outRest2.length() == 4){
			getOutRest2HMM();
		}

		if(outRestHour2>inRestHour2) {
			restHour = outRestHour2 -inRestHour2;
		}else {
			restHour = outRestHour2 +24 -inRestHour2;
		}

		restMinute = outRestMinute2 - inRestMinute2;
		if(restMinute < 0) {
			restMinute += 60;
			restHour -= 1;
		}
	if(restMinute < 10){
		sheet.getRow(i+8).getCell(7).setCellValue(restHour+":0"+restMinute);
	}else {
		sheet.getRow(i+8).getCell(7).setCellValue(restHour+":"+restMinute);
	}
}

	//実働時間の表示
	private void createWorkingTime() {
		if(todayOrNext == 1) {
			if(outTime.length() == 5) {
				getOutHHMM();
			}else if(outTime.length() == 4){
				getOutHMM();
			}
			workingHour = outHour+24 -inHour - totalRestHour;
		}else {
			getOutHHMM();
			workingHour = outHour -inHour - totalRestHour;
		}
		workingMinute = outMinute - inMinute - totalRestMinute;
		if(workingMinute < 0) {
			workingMinute += 60;
			workingHour -= 1;
		}

		if(workingMinute < 10){
			sheet.getRow(i+8).getCell(8).setCellValue(workingHour+":0"+workingMinute);
		}else {
			sheet.getRow(i+8).getCell(8).setCellValue(workingHour+":"+workingMinute);
		}
	}

	//total時間の表示
	private void createTotalCount(int i, int hour, int minute) {
		if (minute<10) {
			sheet.getRow(39).getCell(i).setCellValue(hour+":0"+minute);
		}else {
			sheet.getRow(39).getCell(i).setCellValue(hour+":"+minute);
		}
	}

	private void getNightRest() {
		if((inRestHour2>=22)&&(inRestHour2<=23)) {
			if((outRestHour2>=22)&&outRestHour2<=23) {
				nightRestHour = restHour;
				nightRestMinute = restMinute;
			}else if((outRestHour2>=0)&&(outRestHour2<5)) {
				nightRestHour = restHour;
				nightRestMinute = restMinute;
			}
		}else if((inRestHour2<22)&&(outRestHour2>=22)&&(outRestHour2<=23)) {
			nightRestMinute = outRestHour2 - 22;
			nightRestMinute = restMinute;
		}else if((inRestHour2>=0)&&(inRestHour2<5)&&(outRestHour2<5)) {
			nightRestHour = restHour;
			nightRestMinute = restMinute;
		}else if((inRestHour2>=0)&&(inRestHour2<5)&&(outRestHour2>=5)) {
			nightRestHour = 5 - inRestHour2;
			nightRestHour = 60 - restMinute;

		}
	}

}

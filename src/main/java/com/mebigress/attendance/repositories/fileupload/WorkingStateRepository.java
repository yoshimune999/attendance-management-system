package com.mebigress.attendance.repositories.fileupload;

import com.mebigress.attendance.entity.fileupload.WorkingStateEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkingStateRepository extends JpaRepository<WorkingStateEntity, Integer>{
}

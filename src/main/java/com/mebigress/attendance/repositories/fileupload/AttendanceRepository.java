package com.mebigress.attendance.repositories.fileupload;

import com.mebigress.attendance.entity.fileupload.AttendanceEntity;
import com.mebigress.attendance.entity.fileupload.AttendancePK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttendanceRepository extends JpaRepository<AttendanceEntity, AttendancePK>{

}

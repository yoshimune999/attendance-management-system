package com.mebigress.attendance.repositories.createworkschedule;

import java.sql.Date;

import com.mebigress.attendance.entity.createworkschedule.RelationalEntity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RelationalRepository extends JpaRepository<RelationalEntity, Date>{


}

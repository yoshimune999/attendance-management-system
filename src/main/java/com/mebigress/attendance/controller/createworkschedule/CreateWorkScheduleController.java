package com.mebigress.attendance.controller.createworkschedule;

import java.net.URLEncoder;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.mebigress.attendance.entity.createworkschedule.RelationalEntity;
import com.mebigress.attendance.logic.createworkschedule.GetWrokScheduleLogic;
import com.mebigress.attendance.service.createworkschedule.CreateWrokScheduleService;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CreateWorkScheduleController {

	@Autowired
	private CreateWrokScheduleService service;
	Workbook book;

	@RequestMapping(value = "/downloadindex", method = RequestMethod.GET)
	public ModelAndView downloadindex(ModelAndView mav) {
		mav.setViewName("download/download");
		return mav;
	}

	//勤務表出力
	@RequestMapping(value = "/download", method = RequestMethod.POST)
	public void responseExcel(
			@RequestParam(value="year",required=false) String y,
			@RequestParam(value="month",required=false) String m,
			@RequestParam(value="number",required=false) String n,
			HttpServletResponse response, ModelAndView mav)  throws Exception{

		//params取得
		int year = Integer.valueOf(y);
		int month = Integer.valueOf(m);
		int number = Integer.valueOf(n);


		Calendar cal = Calendar.getInstance();
		cal.set(year,month-1,01);
		int last = cal.getActualMaximum(Calendar.DATE);

		Date beginning = Date.valueOf(year+"-"+month+"-01");
		Date end = Date.valueOf(year+"-"+month+"-"+last);

		List<RelationalEntity> list = service.getPersonalSchedule(beginning, end, number);


		GetWrokScheduleLogic schedule = new GetWrokScheduleLogic();
		book = schedule.getWrokSchedule(list);
		RelationalEntity entity = new RelationalEntity();
		entity = list.get(0);
		String fileName = "000"+entity.getStaffNumber()+entity.getName()+year+month+"メビグレス勤務表、経費精算書、住所等新規変更届書　雛形_修正Ver7.xlsx";

		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition", "attachment; filename=" +URLEncoder.encode(fileName,"UTF-8")+ "");
		response.setCharacterEncoding("UTF-8");
		response.getOutputStream();
		book.write(response.getOutputStream());
    }

}

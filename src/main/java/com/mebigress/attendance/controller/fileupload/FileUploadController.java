package com.mebigress.attendance.controller.fileupload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.mebigress.attendance.logic.fileupload.ReadCSVLogic;
import com.mebigress.attendance.service.fileupload.FileUploadService;


@Controller
public class FileUploadController {

	@Autowired
	private FileUploadService service;

	@RequestMapping(value = "/uploadindex", method = RequestMethod.GET)
	public ModelAndView uploadindex(ModelAndView mav) {
		mav.setViewName("upload/upload");
		return mav;
	}

	//CSVアップロード
	@RequestMapping(value = "/addCSV", method = RequestMethod.POST)
	public ModelAndView insertCSV(
			@RequestParam(value="CSV",required=false) MultipartFile[] csv,
			ModelAndView mav) {

		for(MultipartFile uploadFile: csv) {
		ReadCSVLogic rcl = new ReadCSVLogic();
		rcl.readCSV(uploadFile);

		service.saveWorkingStateEntity(rcl.getWorkingStateEntity());
		service.saveEmployeeEntity(rcl.getEmployeeEntity());
		service.saveAttendanceEntity(rcl.getAttendanceEntity());

		}

		return new ModelAndView("redirect:/");
	}

}

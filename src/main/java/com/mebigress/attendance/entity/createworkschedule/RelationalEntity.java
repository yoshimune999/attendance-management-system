package com.mebigress.attendance.entity.createworkschedule;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class RelationalEntity implements Serializable{
	@Id
	@Column(name="date")
	private Date date;

	@Column(name="staff_number")
	private Integer staffNumber;

	@Column(name="category_id")
	private int categoryId;

	@Column(name="in_time")
	private String inTime;

	@Column(name="today_or_next1")
	private int todayOrNext1;

	@Column(name="out_time")
	private String outTime;

	@Column(name="today_or_next2")
	private int todayOrNext2;

	@Column(name="rest1")
	private String rest1;

	@Column(name="today_or_next3")
	private int todayOrNext3;

	@Column(name="rest1_end")
	private String rest1End;

	@Column(name="today_or_next4")
	private int todayOrNext4;

	@Column(name="rest2")
	private String rest2;

	@Column(name="today_or_next5")
	private int todayOrNext5;

	@Column(name="rest2_end")
	private String rest2_end;

	@Column(name="today_or_next6")
	private int todayOrNext6;

	@Column(name="expense")
	private int expense;

	@Column(name="remarks")
	private String remarks;

	@Column(name="comment")
	private String comment;

	@Column(name="approval_state")
	private String approvalState;

	@Column(name="punch_in_time")
	private String punchInTime;

	@Column(name="punch_out_time")
	private String punchOutTime;

	@Column(name="punch_rest1")
	private String punchRest1;

	@Column(name="punch_rest1_end")
	private String punchRest1End;

	@Column(name="punch_rest2")
	private String punchRest2;

	@Column(name="punch_rest2_end")
	private String punchRest2End;

	@Column(name="staff_name")
	private String name;

	@Column(name="category_name")
	private String categoryName;


}

package com.mebigress.attendance.entity.fileupload;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;

import lombok.Data;


@Data

public class AttendancePK implements Serializable {

	@Column(name="date")
	private Date date;

	@Column(name="staff_number")
	private int staffNumber;



}

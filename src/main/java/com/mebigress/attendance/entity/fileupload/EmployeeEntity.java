package com.mebigress.attendance.entity.fileupload;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data

@Entity
@Table(name="employee")
public class EmployeeEntity {

	@Id
	@Column(name="number")
	private Integer number;


	@Column(name="name")
	private String name;
}

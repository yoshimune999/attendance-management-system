package com.mebigress.attendance.entity.fileupload;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data

@Entity
@Table(name="working_state")
public class WorkingStateEntity implements Serializable  {

	@Id
	@Column(name="id")
	private int id;

	@Column(name="name")
	private String name;

}

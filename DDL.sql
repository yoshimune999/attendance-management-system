-- 勤務区分テーブル
create table working_state (
id int not null primary key,
name varchar unique not null
);

-- 社員テーブル
create table employee (
number int not null primary key,
name varchar not null
);

-- メインテーブル
create table attendance (
date date not null,
staff_number int not null,
category_id int not null,
in_time varchar(50),
today_or_next1 int,
out_time varchar(50),
today_or_next2 int,
rest1 varchar(50),
today_or_next3 int,
rest1_end varchar(50),
today_or_next4 int,
rest2 varchar(50),
today_or_next5 int,
rest2_end varchar(50),
today_or_next6 int,
expense int,
remarks varchar(200),
comment varchar(200),
approval_state varchar(50) not null,
punch_in_time varchar(50),
punch_out_time varchar(50),
punch_rest1 varchar(50),
punch_rest1_end varchar(50),
punch_rest2 varchar(50),
punch_rest2_end varchar(50),
foreign key (staff_number) references employee (number),
foreign key (category_id) references working_state (id)
);

